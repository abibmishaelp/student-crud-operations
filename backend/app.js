require('dotenv').config();
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');

var logger = require('morgan');
let mysql = require("mysql");
let db_template = require("db-template");
let fs = require("fs");
let parser = require("xml-js");


var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

// database connection - mysql config
const pool = mysql.createPool({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT,
  database: process.env.DB_NAME,
  connectionLimit: 500,
  connectTimeout: 50000
});

executeQuery = db_template(pool);
pool.on("connection", connection => {
  console.log("Connection Acquired");
});
pool.on("acquire", connection => {
  console.log("Connection %d acquired", connection.threadId);
});

pool.on("enqueue", () => {
  console.log("Waiting for available connection slot");
});

pool.on("release", connection => {
  console.log("Connection %d released", connection.threadId);
});

let content = fs.readFileSync(__dirname + "/sql-queries.xml");

// let json = parser.xml2json(content, {
//   sanitize: false
// });

// let sqlQueries = JSON.parse(json)["elements"][1].elements;
// sqlQueryMap = {};
// for (let i = 0; i < sqlQueries.length; i++) {
//   if (sqlQueries[i]["attributes"]) {
//     sqlQueryMap[sqlQueries[i]["attributes"]["id"]] =
//       sqlQueries[i]["elements"][0]["cdata"];
//   }
// }

pool.on("error", err => {
  if (err) {
    console.log("appPool Erro", err);
  }
});


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(
  express.urlencoded({
    extended: false
  })
);
let bodyParser = require("body-parser");
app.use(
  bodyParser.urlencoded({
    extended: false
  })
);
app.use(bodyParser.json());

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
